/*INSERT INTO book (title, author, isbn, shortdescription, longdescription, price, discount, shelfposition, shelflevel,
                  thumbnailurl, publisher)
VALUES ('Come non cucinare', 'chef Calmanocciuolo', '3419', 'ricettario', 'Ricettario che ti spiega come cucinare cose semplici', '25', '5', 'A',
        '6', '', '');
INSERT INTO book (title, author, isbn, shortdescription, longdescription, price, discount, shelfposition, shelflevel,
                  thumbnailurl, publisher)
VALUES ('Evoluzione della sscienza spiegata male', 'BarbascuraX', '5861', 'Enciclopedia', 'evoluzione e comportamento di alcune specie sconosciute', '30',
        '3', 'A', '10', '', '');

INSERT INTO users (username, password)
VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6');*/

INSERT INTO authors(name, surname, nickname) VALUES ('Antonino', 'Cannavacciulo', 'chef Calmanocciuolo');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Come non cucinare', '3419','ricettario', 'chef Calmanocciuolo', '2021 - 03 - 12', 25, 5);
INSERT INTO book_author(book_id, author_id) VALUES (1, 1);

INSERT INTO authors(name, surname, nickname) VALUES ('Barbascura', 'X', 'Capitano');
INSERT INTO books(title, isbn, abstract, publisher, published_date, price, discount) VALUES ('Evoluzione della sscienza spiegata male', '5861', 'Enciclopedia', 'BarbascuraX', '2023 - 08 - 11', 18, 3);
INSERT INTO book_author(book_id, author_id) VALUES (2, 2);

INSERT INTO users(username, password) VALUES ('admin', '$2a$10$yxZjf/A5ndtHO7zfZ3HsbuG7AZ2RCCMn75f7FoOO1E2x2/BtJFiK6' );