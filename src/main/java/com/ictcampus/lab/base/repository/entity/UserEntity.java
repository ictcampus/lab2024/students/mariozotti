package com.ictcampus.lab.base.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import jakarta.persistence.Table;
import lombok.Data;

@Data
@Table(name = "users")
@Entity
public class UserEntity {
    @Id
    @GeneratedValue
    private Long id;

    private String username;
    private String password;

    /*public long getUsername() {
        return username;
    }

    public void setUsername( final long id ) {
        this.username = username;
    }

    public long getPassword() {
        return password;
    }

    public void setPassword( final long password ) {
        this.password = password;
    }*/

}
