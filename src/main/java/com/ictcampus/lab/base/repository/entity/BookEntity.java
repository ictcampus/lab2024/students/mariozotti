package com.ictcampus.lab.base.repository.entity;

import jakarta.persistence.Entity;
import jakarta.persistence.Id;

/**
 * TODO Add Class Description
 *
 * @author Emilio (FEj) Frusciante - Beta80Group
 * @since 1.0.0
 */

@Entity
public class BookEntity {
    @Id
    private long id;

    private String title;
    private String author;

    public long getId() {
        return id;
    }

    public void setId( final long id ) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    /*public void setTitle( final title ) {
        this.title = title;
    }*/

    public String getAuthor() {
        return author;
    }

    public void setAuthor( final String author ) {
        this.author = author;
    }
}
