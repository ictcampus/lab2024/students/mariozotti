/*package com.ictcampus.lab.base.repository;

import com.ictcampus.lab.base.repository.entity.BookEntity;
import com.ictcampus.lab.base.repository.entity.BookEntity;
import lombok.AllArgsConstructor;
import org.springdoc.core.properties.SwaggerUiOAuthProperties;
import org.springframework.jdbc.core.BeanPropertyRowMapper;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCreator;
import org.springframework.jdbc.core.PreparedStatementCreatorFactory;
import org.springframework.jdbc.support.GeneratedKeyHolder;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import java.sql.Types;
import java.util.Arrays;
import java.util.List;


@AllArgsConstructor
@Repository
public class BookRepository {
    private final SwaggerUiOAuthProperties swaggerUiOAuthProperties;
    private JdbcTemplate jdbcTemplate;

    public List<BookEntity> findAll() {
        return jdbcTemplate.query( "SELECT * FROM book", new BeanPropertyRowMapper<>( BookEntity.class ) );
    }

    public BookEntity findById( long id ) {
        return jdbcTemplate.queryForObject( "SELECT * FROM book WHERE id = ?",
                new BeanPropertyRowMapper<>(BookEntity.class ), id );
    }

    public BookEntity findByName( String name ) {
        return jdbcTemplate.queryForObject( "SELECT * FROM book WHERE name = ?",
                new BeanPropertyRowMapper<>( BookEntity.class ), name );
    }

    public List<BookEntity> findByNameOrSearch( String title, String search ) {
        String filter = title;
        if ( filter == null || filter.isEmpty() ) {
            filter = search;
        }

        filter = "%" + filter + "%";
        return jdbcTemplate.query( "SELECT * FROM book WHERE name ILIKE ? OR system ILIKE ?",
                new BeanPropertyRowMapper<>( BookEntity.class ), filter, filter );
    }

    @Transactional
    public long create( String title, String author ) {
        GeneratedKeyHolder generatedKeyHolder = new GeneratedKeyHolder();
        PreparedStatementCreatorFactory preparedStatementCreatorFactory = new PreparedStatementCreatorFactory(
                "INSERT INTO world (title, auhtor) VALUES (?, ?)",
                Types.VARCHAR, Types.VARCHAR
        );
        preparedStatementCreatorFactory.setReturnGeneratedKeys( true );

        PreparedStatementCreator preparedStatementCreator = preparedStatementCreatorFactory
                .newPreparedStatementCreator(
                        Arrays.asList( title )
                );


        jdbcTemplate.update( preparedStatementCreator, generatedKeyHolder );

        return generatedKeyHolder.getKey().longValue();
    }

    public int update( long id, String title, String author ) {
        return jdbcTemplate.update( "UPDATE book SET title=?, author=? WHERE id=?", title, author, id );
    }

    public int delete( long id ) {
        return jdbcTemplate.update( "DELETE FROM book WHERE id=?", id );
    }
}
*/