package com.ictcampus.lab.base.repository;

import com.ictcampus.lab.base.repository.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {
    UserEntity findByUsername(final String username);

    Boolean existsByUsername(final String username);
}
