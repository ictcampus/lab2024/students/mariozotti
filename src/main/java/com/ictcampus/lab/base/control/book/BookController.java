package com.ictcampus.lab.base.control.book;

import com.ictcampus.lab.base.control.book.mapper.BookControllerStructMapper;
import com.ictcampus.lab.base.control.book.model.BookResponse;
import com.ictcampus.lab.base.service.book.BookService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping( "/api/v1/books" )
@AllArgsConstructor
@Slf4j
public class BookController {
	@Autowired
	private BookService bookService;

	@Autowired
	private BookControllerStructMapper bookControllerStructMapper;

	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooks() {
		return bookControllerStructMapper.toBooks(bookService.getBooks());
	}

	@GetMapping( value = "/custom", produces = { MediaType.APPLICATION_JSON_VALUE } )
	public List<BookResponse> getBooksCustom() {
		return bookControllerStructMapper.toBooks(bookService.getBooksCustom());
	}
}

//import com.ictcampus.lab.base.control.model.BookResponse;
//import com.ictcampus.lab.base.control.model.BookRequest;
//import lombok.AllArgsConstructor;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.data.crossstore.ChangeSetPersister;
//import org.springframework.http.MediaType;
//import org.springframework.web.bind.annotation.*;
//
//import java.util.ArrayList;
//import java.util.Arrays;
//import java.util.List;
//import java.util.Optional;
//
//
//@RestController
//@RequestMapping( "/api/v1/books" )
//@AllArgsConstructor
//public class BookController {
//	private static final Logger log = LoggerFactory.getLogger(BookController.class);
//	private List<BookResponse> list = new ArrayList<>();
//
//	public BookController() {this.list = generateBooks();}
//
//	private List<BookResponse> generateBooks() {
//		return null;
//	}
//
//	//private static final Logger log = LoggerFactory.getLogger(BookController.class);
//
//	@GetMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
//	public List<BookResponse> getBooks() {
//		log.info("Restituisco la lista dei libri [{}]", list);
//		return this.list;
//
//		/*List<BookResponse> list = new ArrayList<>();
//
//		return generateBooks(list);*/
//	}
//
//	private static List<BookResponse> generateBooks(List<BookResponse> list) {
//		BookResponse bookResponse = new BookResponse();
//		bookResponse.setId( 1L );
//		bookResponse.setTitle( "Le guerre del mondo emerso" );
//		bookResponse.setAuthor( "Licia Troisi" );
//		bookResponse.setISDB( "8014366120588" );
//		bookResponse.setGender("Narrativa fantasy");
//		bookResponse.setPrice(15L);
//
//		BookResponse bookResponse1 = new BookResponse();
//		bookResponse1.setId( 2L );
//		bookResponse1.setTitle( "Il libro dei cocktails" );
//		bookResponse1.setAuthor( "Paul Russo" );
//		bookResponse1.setISDB( "8840368949" );
//		bookResponse1.setGender("Ricettario");
//		bookResponse1.setPrice(10L);
//
//		list.add( bookResponse );
//		list.add( bookResponse1 );
//		return list;
//	}
//
//	@GetMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
//	public BookResponse getBook (
//			@PathVariable(name = "id") Long id
//	) throws ChangeSetPersister.NotFoundException {
//		BookResponse bookResponse = this.findBookById(id);
//		if (bookResponse == null) {
//			log.info("Libro con ID [{}] non trovato", id);
//			throw new ChangeSetPersister.NotFoundException();
//	}
//
//	log.info("Restituisco il libro con ID [{}] e dati [{}]", id, bookResponse);
//	return  bookResponse;
//
//	/*public List<BookResponse> getBooks(@PathVariable(name = "id") Long id
//	) {
//		List<BookResponse> list = new ArrayList<>();
//
//		BookResponse bookResponse = new BookResponse();
//		bookResponse.setId( 1L );
//		bookResponse.setTitle( "Le guerre del mondo emerso" );
//		bookResponse.setAuthor( "Licia Troisi" );
//		bookResponse.setISDB( "8014366120588" );
//		bookResponse.setGender("Narrativa fantasy");
//		bookResponse.setPrice(15L);
//
//		BookResponse bookResponse1 = new BookResponse();
//		bookResponse1.setId( 2L );
//		bookResponse1.setTitle( "Il libro dei cocktails" );
//		bookResponse1.setAuthor( "Paul Russo" );
//		bookResponse1.setISDB( "8840368949" );
//		bookResponse1.setGender("Ricettario");
//		bookResponse1.setPrice(10L);
//
//		//list.add( bookResponse );
//		list.add( bookResponse1 );
//		return list;
//	  }*/
//	}
//
//
//	@PostMapping( value = "", produces = { MediaType.APPLICATION_JSON_VALUE } )
//	public Long createBooks(
//			@RequestBody BookRequest bookRequest
//
//	) {
//		log.info("Creo un nuovo libro con i dati [{}]", bookRequest);
//
//		Long id = this.lastId();
//		log.info("ID del nuovo piabeta con i dati [{}]", id);
//
//		BookResponse bookResponse = new BookResponse();
//		bookResponse.setId( 1L );
//		bookResponse.setTitle( bookRequest.getTitle() );
//		bookResponse.setAuthor( bookRequest.getAuthor() );
//		bookResponse.setISDB( bookRequest.getISDB() );
//		bookResponse.setPrice( bookRequest.getPrice() );
//
//		return bookResponse.getId();
//	}
//
//	@PutMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
//	public void editBook(
//			@PathVariable(name = "id") Long id,
//			@RequestBody BookRequest bookRequest
//
//	) {
//		log.info("Agiorno il libro con ID [{}] e dati [{}]", id, bookRequest);
//		this.updateById(id, bookRequest);
//		/*BookResponse bookResponse = new BookResponse();
//		bookResponse.setId( id );
//		bookResponse.setTitle( bookRequest.getTitle() );
//		bookResponse.setAuthor( bookRequest.getAuthor() );
//		bookResponse.setGender( bookRequest.getGender() );
//		bookResponse.getPrice( );*/
//
//	}
//
//	@DeleteMapping( value = "/{id}", produces = { MediaType.APPLICATION_JSON_VALUE } )
//	public void deleteBook(
//			@PathVariable(name = "id") Long id
//	) {
//		log.info("Cancello il libro con ID [{}]", id);
//
//		/*BookResponse bookResponse = new BookResponse();
//		bookResponse.setId( id );
//		bookResponse.setTitle( bookResponse.getTitle() );
//		bookResponse.setAuthor( bookResponse.getAuthor() );
//		bookResponse.setISDB( bookResponse.getISDB() );
//		bookResponse.setGender( bookResponse.getGender() );
//		bookResponse.setPrice( bookResponse.getPrice() );*/
//	}
//
//	private List<BookResponse> genrateBooks() {
//		String formato = "Fisico";
//		Long index = 1L;
//		List<BookResponse> listTmp = new ArrayList<>(Arrays.asList(
//				new BookResponse(index, "Le guerre del mondo emerso", formato),
//				new BookResponse(++index, "Il libro dei cocktails", formato)
//		));
//		return listTmp;
//	}
//
//	private BookResponse findBookById(Long id) {
//		for (BookResponse bookResponse : this.list) {
//			if (bookResponse.getId().equals(id)){
//				return  bookResponse;
//			}
//		}
//		return null;
//	}
//
//
//	private BookResponse streamFindBookById(Long id){
//		Optional<BookResponse> bookResponseOptional = this.list.stream().filter(item -> item.getId().equals(id)).findFirst();
//
//		if (bookResponseOptional.isPresent()) {
//			return bookResponseOptional.get();
//		}
//		return null;
//	}
//
//	private Long lastId() {
//		Long lastIdex = 1L;
//		for (BookResponse bookResponse : this.list)  {
//			if (bookResponse.getId() >= lastIdex) {
//				lastIdex = bookResponse.getId() + 1;
//			}
//		}
//		return lastIdex;
//	}
//
//	private void updateById(Long id, BookRequest newData) {
//		for (BookResponse bookResponse : this.list) {
//			if (bookResponse.getId().equals(id)) {
//				bookResponse.setTitle(newData.getTitle());
//				bookResponse.setGender(newData.getGender());
//				bookResponse.setAuthor(newData.getAuthor());
//			}
//		}
//	}
//	private void deleteById(Long id) {this.list.removeIf(item -> item.getId().equals(id));}
//}
