package com.ictcampus.lab.base.control.authentication.model;

import lombok.Value;
import lombok.extern.jackson.Jacksonized;

@Value
@Jacksonized
public class AuthenticationRequest {
    String username;
    String password;
}
